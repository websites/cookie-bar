module.exports = {
  parser: "@babel/eslint-parser",
  extends: ["eslint:recommended", "prettier"],
  parserOptions: {
    sourceType: "module",
    requireConfigFile: false,
  },
  env: {
    browser: true,
    es2021: true,
    node: true,
  },
};

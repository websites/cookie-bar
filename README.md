# Cookie Bar

![screenshot](https://gitlab.nic.cz/uploads/-/system/personal_snippet/1421/8fc8d07a235a2d43ac8d2be7d9db02e8/chickadee_2021-12-01_14_40_42.png)

[demo page](https://websites.pages.nic.cz/cookie-bar/)

## Features

- multiple language support, with autodetection from document & browser
- top or bottom position on the webpage, mobile viewport support
- (optionally) removes non-essential cookies when the user does not give a consent to store them
- (optionally) replaces Youtube `<iframe>` URLs (`youtube.com -> youtube-nocookie.com`)
- (optionally) disables Piwik/Matomo and Google Tag Manager / Analytics cookies
- automatically loads CSS from an external file to comply with strict CSP settings
- consent status is stored into a cookie, so even server-side apps can read it and take measures, and there's a a wrapper function to read it easily in the browser
- pretty small, about 2-3 kB with gzip/brotli compression
- uBlock/adBlock and other cookie-bar-destroying-addon friendly

## Usage

Cookie bar is available in `window.nicWidgets`, same as [omnibar-ng](https://gitlab.nic.cz/websites/omnibar-ng/):

```js
window.cookieBar = new window.nicWidgets.cookieBar(options);
```

It's recommended to run it before `DOMContentLoaded` fires, so it can change Youtube iframe URLs and Piwik/GTM scripts before they load:

```html
<script
  type="text/javascript"
  src="https://websites.pages.nic.cz/cookie-bar/cookiebar.js"
  onload="window.cookieBar = new nicWidgets.cookieBar()"
></script>
```

If you just want to give it a quick try on your website, run this in the JS console (if CSP on the website allows it):

```js
const script = document.createElement("script");
script.src = "https://websites.pages.nic.cz/cookie-bar/cookiebar.js";
script.onload = () => (window.cookieBar = new window.nicWidgets.cookieBar());
document.head.appendChild(script);
```

Note: if you get error messages like `Loading failed for the <script> with source “https://websites.pages.nic.cz/cookie-bar/cookiebar.js”.`, uBlock or similar addon might be blocking it…

The `cookieBar` constructor accepts a configuration object. Defaults are:

```js
cookieBar({
  language: "…", // text language, default is autodetected from the document and browser, "en" and "cs" are supported with "en" being used as a fallback
  automount: true, // show the bar after the script is loaded without calling .mount() explicitly
  position: "bottom", // position on the page, "top" or "bottom"
  moreInfoUrl: "https://nic.cz/cookies", // URL for the "more info" link/button
  changeYoutubeUrls: true, // replace URLs in Youtube iframes to the nocookie domain
  disableMatomoCookies: true, // change the Matomo script to disable cookies if no consent is given
  disableGtagCookies: true, // change the Google Tag Manager script cookies to disable cookies if no consent is given
  defaultConsent: undefined, // if set to true/false, act as if the consent was/wasn't given and don't show the bar (can be opened with `cookieBar.reopen()`)
  consentCookieExpire: 365 * 5, // how long (days) is the cookie consent cookie stored
  removeCookies: true, // try to remove non-essential cookies when the uses clicks "No" or when the cookie bar detects the consent cookie is already set to "false"
  preservedCookieNames: [
    // list of essential cookies that shouldn't be removed
    // items can be either "strings" or /regular.*expressions/i
    // the consent cookie is automatically added to this list
    "csrftoken",
    "sessionid",
    "mojeidsession",
    "auth_method",
    "identifier",
  ],
});
```

the bar and button strings can be forced to custom values:

```js
cookieBar({
  strings: {
    jump: "Jump to content",
    text: "…",
    buttonYes: "Yes",
    buttonNo: "No",
    buttonMore: "More info",
    newTab: "Link will open in a new browser window or tab",
  },
});
```

(missing keys will be filled in with values from the fallback language in `config.js`)

## API

### `.mount()`

Inserts the bar to `document.body` (beginning or end depending on `position`) and CSS link to `document.head`. Runs automatically after the script is loaded with the default `automount` option. Does nothing when the bar is mounted already.

### `.unmount()`

Removes the bar element from `document.body` and CSS from `document.head`. Does nothing when it's already unmounted.

### `.switchLanguage(lang)`

Switches string language. You probably don't want to do this if you want to keep the forced strings set using `cookieBar({strings: { … }})`, as they are not remembered and it's not possible to switch back to them.

### `switchPosition(position)`

Switches the bar to top or bottom position on the page. Does nothing if the bar is there already.

### `.getConsentCookie()`

Gets the consent cookie value:

- `true` - user clicked 'Yes'
- `false` - user clicked 'No'
- `undefined` - no cookie yet

Of course it's possible to read it from `document.cookies` too, but the browser API is kinda dumb and [`CookieStore`](https://developer.mozilla.org/en-US/docs/Web/API/CookieStore) is not yet widely supported.

Using `options.defaultConsent` doesn't affect this, it reflects just the actual consent given (or not) by the user using the Yes/No buttons.

### `.reopen()`

Reopens the bar to allow consent change.

```html
<button onclick="cookieBar.reopen()">Nastavení cookies</button>
```

### `.options`

Object with current bar options

### `.removedCookies`

List of removed cookie names

## Building & development

Cookie bar uses [esbuild](https://esbuild.github.io/) and [PostCSS](https://postcss.org/). You can run both using `npm run build`:

```
> cookie-bar@1.0.0 build
> rm -rf public/* && cp src/index.html public && esbuild src/cookiebar.js --bundle --minify --sourcemap --loader:.svg=text --target=firefox93,chrome95 --outdir=public && postcss src/cookiebar.scss --parser postcss-scss --use postcss-mixins --use postcss-nested --use postcss-simple-vars --use autoprefixer --use cssnano -m -o public/cookiebar.css


  public/cookiebar.js       8.2kb
  public/cookiebar.js.map  21.2kb

⚡ Done in 5ms
```

There's no devserver, live reload nor HMR configured, as esbuild is very fast (compared to Webpack etc.). Automatic rebuilding after source changes can be done with inotify (on Linux):

```bash
while inotifywait -e close_write src/*; do
    npm run build;
done
```

Some options are set at build time, see `config.js`.
